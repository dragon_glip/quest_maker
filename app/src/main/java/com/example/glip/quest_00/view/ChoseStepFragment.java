package com.example.glip.quest_00.view;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.adapter.StepChoseItemAdapter;
import com.example.glip.quest_00.helper.OnStartDragListener;
import com.example.glip.quest_00.helper.SwipeAndDragHelper;
import com.example.glip.quest_00.helper.SwipeControllerActions;
import com.example.glip.quest_00.utils.SpacesItemDecoration;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

abstract class ChoseStepFragment extends Fragment implements StepChoseItemAdapter.ChoseStepItemAdapterListener, OnStartDragListener {
    StepChoseItemAdapter stepChoseItemAdapter;
    @BindView(R.id.recyclerView3)
    RecyclerView recyclerView;
    @BindView(R.id.quest_name)
    TextView questName;
    @BindView(R.id.button2)
    Button newStep;
    private Unbinder unbinder;
    FragmentManager fragmentManager;
    protected QuestViewModel viewModel;
    View rootView;
    MyDeleteDialog createDialog;
    ItemTouchHelper mItemTouchHelper;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.chose_step_fragment, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(QuestViewModel.class);
        }
        questName.setText(viewModel.getQuestName());
        initializationRecyclerView();
        newStep.setOnClickListener(view -> createNewStep());
        return rootView;
    }

    private void createNewStep() {
        viewModel.setCurrentIdQuestStep(0);
        QuestStepFragment questStepFragment = new QuestStepFragment();
        questStepFragment.setFragmentManager(fragmentManager);
        fragmentManager.beginTransaction()
                .remove(this)
                .add(R.id.fragment_container, questStepFragment)
                .addToBackStack(null)
                .commit();
    }

    protected void initializationRecyclerView() {
        stepChoseItemAdapter = new StepChoseItemAdapter(this, this);
        stepChoseItemAdapter.submitList(viewModel.getStepListByQuestId());

        recyclerView.setAdapter(stepChoseItemAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        ItemTouchHelper.Callback touchHelper = new SwipeAndDragHelper(stepChoseItemAdapter,getActivity());
//        final SwipeAndDragHelper touchHelper = new SwipeAndDragHelper(stepChoseItemAdapter, new SwipeControllerActions() {
//            @Override
//            public void onLeftClicked(int position) {
//                super.onLeftClicked(position);
//            }
//
//            @Override
//            public void onRightClicked(int position) {
//                super.onRightClicked(position);
//            }
//        });


        mItemTouchHelper = new ItemTouchHelper(touchHelper);
        mItemTouchHelper.attachToRecyclerView(recyclerView);

//        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
//            @Override
//            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
//                touchHelper.onDraw(c);
//            }
//        });

    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

}
