package com.example.glip.quest_00;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.glip.quest_00.db.DataBaseHelper;
import com.example.glip.quest_00.db.entity.UsersEntity;

public class App extends Application {
    private static App instance;
    private DataBaseHelper db;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        synchronized (DataBaseHelper.class) {
            db = Room.databaseBuilder(getApplicationContext(), DataBaseHelper.class, "myDataBase09")
                    .allowMainThreadQueries()
                    .build();
        }

    }

    public DataBaseHelper getDatabaseInstance() {
        return db;
    }
}
