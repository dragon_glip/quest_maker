package com.example.glip.quest_00.model;

import java.util.Date;

public interface Steps {
    int getStepId();
    long getQuestId();
    Date getDateCreated();
    Date getDateModified();
    String getCondition();
}
