package com.example.glip.quest_00.helper;

public interface ItemTouchHelperViewHolder {
    void onItemSelected();
    void onItemClear();
}
