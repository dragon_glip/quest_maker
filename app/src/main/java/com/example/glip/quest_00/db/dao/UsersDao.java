package com.example.glip.quest_00.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.glip.quest_00.db.entity.UsersEntity;

import java.util.List;

@Dao
public interface UsersDao {
    @Insert
    void insert(UsersEntity usersEntity);

    @Delete
    void delete(UsersEntity usersEntity);

    @Query("SELECT * FROM USERS")
    List<UsersEntity> getAllData();

    //пример запроса с выборкой
    @Query("SELECT * FROM users WHERE name LIKE :name")
    UsersEntity getByName(String name);

    @Query("SELECT * FROM users WHERE id = :id")
    UsersEntity getById (int id);
}
