package com.example.glip.quest_00.view;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.glip.quest_00.App;
import com.example.glip.quest_00.R;
import com.example.glip.quest_00.db.DataBaseHelper;
import com.example.glip.quest_00.db.entity.UsersEntity;

public class FragmentContainer extends AppCompatActivity {
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);


        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setName("admin");
        usersEntity.setPassword("admin");
        DataBaseHelper db = App.getInstance().getDatabaseInstance();
        db.getUsersDao().insert(usersEntity);


        fragmentManager = getSupportFragmentManager();
        CaseQuestsFragment caseQuestsFragment = new CaseQuestsFragment();
        caseQuestsFragment.setFragmentManager(fragmentManager);
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, caseQuestsFragment)
                .commit();


    }

}
