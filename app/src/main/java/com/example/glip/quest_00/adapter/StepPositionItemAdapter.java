package com.example.glip.quest_00.adapter;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.example.glip.quest_00.databinding.StepItemCardBinding;
import com.example.glip.quest_00.R;
import com.example.glip.quest_00.db.entity.StepPositionEntity;
import com.example.glip.quest_00.holder.StepItemHolder;
import java.util.List;


public class StepPositionItemAdapter extends RecyclerView.Adapter<StepItemHolder> {

    private LayoutInflater layoutInflater;
    private List<StepPositionEntity> stepPositionEntities;
    private StepItemAdapterListener listener;

    public StepPositionItemAdapter(StepItemAdapterListener listener){
        this.listener = listener;
    }


    @NonNull
    @Override
    public StepItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        StepItemCardBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.step_item_card, viewGroup, false);
        return new StepItemHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull StepItemHolder stepItemHolder, int i) {

        stepItemHolder.binding.setStepPositionEntity(stepPositionEntities.get(i));

        stepItemHolder.binding.deletePosition.setOnClickListener(v -> {
            if (listener != null) {
                listener.onDeleteStepPositionClicked(stepPositionEntities.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return stepPositionEntities != null ? stepPositionEntities.size() : 0;
    }

    public void submitList(List<StepPositionEntity> stepPositionEntities) {
        this.stepPositionEntities = stepPositionEntities;
    }
    public interface StepItemAdapterListener {
        void onDeleteStepPositionClicked(StepPositionEntity stepPositionEntity);

    }
}
