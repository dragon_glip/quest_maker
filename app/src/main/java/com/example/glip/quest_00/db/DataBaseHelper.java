package com.example.glip.quest_00.db;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.example.glip.quest_00.db.converter.DateConverter;
import com.example.glip.quest_00.db.dao.QuestsDao;
import com.example.glip.quest_00.db.dao.StepPositionDao;
import com.example.glip.quest_00.db.dao.StepsDao;
import com.example.glip.quest_00.db.dao.UsersDao;
import com.example.glip.quest_00.db.entity.QuestsEntity;
import com.example.glip.quest_00.db.entity.StepPositionEntity;
import com.example.glip.quest_00.db.entity.StepsEntity;
import com.example.glip.quest_00.db.entity.UsersEntity;

@TypeConverters({DateConverter.class})
@Database(entities = { UsersEntity.class, QuestsEntity.class, StepsEntity.class, StepPositionEntity.class}
                        , version = 1, exportSchema = false)
public abstract class DataBaseHelper extends RoomDatabase {

    public abstract UsersDao getUsersDao();
    public abstract QuestsDao getQuestsDao();
    public abstract StepsDao getStepsDao();
    public abstract StepPositionDao getStepPositionDao();
    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }


}
