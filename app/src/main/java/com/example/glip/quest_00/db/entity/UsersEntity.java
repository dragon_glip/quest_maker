package com.example.glip.quest_00.db.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.example.glip.quest_00.model.Users;

@Entity(tableName = "users")
public class UsersEntity implements Users {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private String name;
    private String password;

    public UsersEntity (){}

    public int getId() {
        return id;
    }

    @Override
    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    @Override
    public String getPassword() {return password;}

    public void setPassword(String password) {this.password = password;}


}
