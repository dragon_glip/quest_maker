package com.example.glip.quest_00.model;

import java.util.Date;

public interface Quests {
    long getId();
    int getUserId();
    String getTitle();
    int getTheme();
    Date getDateCreatedQuest();
    Date getDateModifiedQuest();
}
