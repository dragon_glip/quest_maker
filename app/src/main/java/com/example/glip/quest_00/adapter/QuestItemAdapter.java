package com.example.glip.quest_00.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.databinding.QuestItemBinding;
import com.example.glip.quest_00.db.entity.QuestsEntity;
import com.example.glip.quest_00.holder.QuestItemHolder;

import java.util.List;

public class QuestItemAdapter extends RecyclerView.Adapter<QuestItemHolder> {
    private LayoutInflater layoutInflater;
    private List<QuestsEntity> questsEntityList;
    private QuestItemAdapterListener listener;

    public QuestItemAdapter (QuestItemAdapterListener listener){

        this.listener = listener;
    }
    @NonNull
    @Override
    public QuestItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        QuestItemBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.quest_item, viewGroup, false);
        return new QuestItemHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull QuestItemHolder questItemHolder, int i) {
        questItemHolder.binding.setQuestsEntity(questsEntityList.get(i));
        questItemHolder.binding.changeQuest.setOnClickListener(v -> {
            if (listener != null) {
                listener.onQuestClicked(questsEntityList.get(i));
            }
        });
        questItemHolder.binding.deleteQuest.setOnClickListener(v -> {
            if (listener != null) {
                listener.onQuestDeleteClicked(questsEntityList.get(i));
            }
        });
        questItemHolder.binding.playQuest.setOnClickListener(v -> {
            if (listener != null) {
                listener.onQuestPlayClicked(questsEntityList.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return questsEntityList != null ? questsEntityList.size() : 0;
    }

    public void submitList(List<QuestsEntity> questsEntities) {
        this.questsEntityList = questsEntities;
    }

    public interface QuestItemAdapterListener {
        void onQuestClicked(QuestsEntity questsEntity);
        void onQuestPlayClicked(QuestsEntity questsEntity);
        void onQuestDeleteClicked(QuestsEntity questsEntity);
    }
}
