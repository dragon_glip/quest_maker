package com.example.glip.quest_00.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.example.glip.quest_00.BR;
import com.example.glip.quest_00.model.Quests;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "quests",
        foreignKeys = {@ForeignKey(entity = UsersEntity.class, parentColumns = "id", childColumns = "user_id", onDelete = CASCADE)},
        indices = {@Index(value = "user_id")
        }
)
public class QuestsEntity extends BaseObservable implements Quests {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "user_id")
    private int userId;

    private String title;
    private int theme;
    private Date dateCreatedQuest;
    private Date dateModifiedQuest;


    public QuestsEntity (){}

    public QuestsEntity(int userId, String titleQuest, int themeQuest,
                        Date dateCreatedQuest, Date dateModifiedQuest) {
        this.userId = userId;
        this.title = titleQuest;
        this.theme = themeQuest;
        this.dateCreatedQuest = dateCreatedQuest;
        this.dateModifiedQuest = dateModifiedQuest;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int user_id) {
        this.userId = user_id;
    }

    @Override
    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Override
    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    @Override
    public Date getDateCreatedQuest() {
        return dateCreatedQuest;
    }

    public void setDateCreatedQuest(Date dateCreated) {
        this.dateCreatedQuest = dateCreated;
    }

    @Override
    public Date getDateModifiedQuest() {
        return dateModifiedQuest;
    }

    public void setDateModifiedQuest(Date dateModified) {
        this.dateModifiedQuest = dateModified;
    }


}
