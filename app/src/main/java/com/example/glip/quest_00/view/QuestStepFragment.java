package com.example.glip.quest_00.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.adapter.StepPositionItemAdapter;
import com.example.glip.quest_00.db.entity.StepPositionEntity;
import com.example.glip.quest_00.db.entity.StepsEntity;
import com.example.glip.quest_00.utils.MediaManager;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

import java.io.File;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

public class QuestStepFragment extends Fragment implements StepPositionItemAdapter.StepItemAdapterListener {
    @BindView(R.id.radioGroupSetQuestion)
    RadioGroup radioGroupSetQuestion;
    @BindView(R.id.audio_file)
    RadioButton audioFile;
    @BindView(R.id.video_file)
    RadioButton videoFile;
    @BindView(R.id.picture_file)
    RadioButton pictureFile;
    @BindView(R.id.text_file)
    RadioButton textFile;
    @BindView(R.id.download_file)
    Button downloadFile;
    @BindView(R.id.variable_next_step)
    EditText conditionForNextStep;
    @BindView(R.id.save)
    Button saveStep;
    @BindView(R.id.save_and_create_step)
    Button saveAndCreateNewStep;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    StepPositionItemAdapter stepPositionItemAdapter;
    @BindView(R.id.title_quest)
    TextView titleQuest;
    @BindView(R.id.title_step)
    EditText titleStep;
    @BindView(R.id.imageView4)
    ImageView imageView;
    @BindView(R.id.step_for_start)
    TextView stepForStart;
    View rootView;
    Bitmap bitmap;
    @BindView(R.id.choose_previous_step)
    Button choosePreviousStep;
    int caseCode = -1;
    int AUDIO_FILE_CASE = 0;
    int VIDEO_FILE_CASE = 1;
    int PICTURE_FILE_CASE = 2;
    int TEXT_FILE_CASE = 3;
    private Unbinder unbinder;
    private QuestViewModel viewModel;
    MyDeleteDialog createDialog;
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    FragmentManager fragmentManager;
    MediaManager mediaManager;
    private final static int TAKE_PICTURE = 1;
    private final static int TAKE_VIDEO = 45;

    public QuestStepFragment() {
    }

    public static QuestStepFragment newInstance() {
        return new QuestStepFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mediaManager = new MediaManager(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.create_step, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(QuestViewModel.class);
        }
        initializationRecyclerView();

        saveStep.setOnClickListener(view -> saveOrUpdateStep());
        downloadFile.setOnClickListener(v -> downloadFile(caseCode));
        return rootView;
    }

    private void initializationRecyclerView() {
        stepPositionItemAdapter = new StepPositionItemAdapter(this);
        stepPositionItemAdapter.submitList(viewModel.getStepPositionList());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(stepPositionItemAdapter);

    }

    private void saveOrUpdateStep() {
        if (TextUtils.isEmpty(titleStep.getText().toString()) || TextUtils.isEmpty(conditionForNextStep.getText().toString())) {
            Toast.makeText(getContext(), "Проверьте наличие названия шага и условия перехода к следующему шагу.", Toast.LENGTH_SHORT).show();
        } else {
            viewModel.saveOrUpdateQuestStep(titleStep.getText().toString(), conditionForNextStep.getText().toString());
            if (viewModel.getStepListByQuestId().size() < 1) {
                viewModel.setPreviousStepId(0);
            }

            ChoseStepFragmentPlay choseStepFragment = new ChoseStepFragmentPlay();
            choseStepFragment.setFragmentManager(fragmentManager);
            fragmentManager.beginTransaction()
                    .remove(this)
                    .add(R.id.fragment_container, choseStepFragment)
                    .commit();
        }


    }

    @Override
    public void onDeleteStepPositionClicked(StepPositionEntity stepPositionEntity) {
        createDialog = new MyDeleteDialog();
        createDialog.createDialog(getContext(), stepPositionEntity, viewModel); //создаем алерт диалог для подтверждения удаления из базы
        initializationRecyclerView();

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        stepPositionItemAdapter.submitList(viewModel.getStepPositionList());
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        titleQuest.setText(viewModel.getQuestName());
        if (viewModel.currentIdQuestStep != 0) {
            mDisposable.add(viewModel.getStepNameFlow()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(stepTitle -> titleStep.setText(stepTitle),
                            throwable -> Log.e(TAG, "Unable to update username", throwable)));
        }

        if (viewModel.currentIdQuestStep != 0) {
            mDisposable.add(viewModel.getStepConditionFlow()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(condition -> conditionForNextStep.setText(condition),
                            throwable -> Log.e(TAG, "Unable to update username", throwable)));
        }

        if (viewModel.getStepListByQuestId().size() < 1) {
            stepForStart.setText("Первый шаг квеста");
            choosePreviousStep.setClickable(false);
        }

        radioGroupSetQuestion.setOnCheckedChangeListener((radioGroup, checkedId) -> {
            switch (checkedId) {
                case R.id.audio_file:
                    caseCode = AUDIO_FILE_CASE;
                    break;
                case R.id.video_file:
                    caseCode = VIDEO_FILE_CASE;
                    break;
                case R.id.picture_file:
                    caseCode = PICTURE_FILE_CASE;
                    break;
                case R.id.text_file:
                    caseCode = TEXT_FILE_CASE;
                    break;
            }
        });
    }

    @OnClick({R.id.preview, R.id.choose_previous_step, R.id.save_and_create_step})
    void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.preview:
                PreviewFragment previewFragment = new PreviewFragment();
                previewFragment.setFragmentManager(fragmentManager);
                previewFragment.setPreview(true);
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .remove(this)
                        .add(R.id.fragment_container, previewFragment)
                        .commit();
                break;
            case R.id.choose_previous_step:
                ChoseStepFragmentPlay choseStepFragmentPlay = new ChoseStepFragmentPlay();
                choseStepFragmentPlay.setFragmentManager(fragmentManager);
                fragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .remove(this)
                        .add(R.id.fragment_container, choseStepFragmentPlay)
                        .commit();
                break;
            case R.id.save_and_create_step:
                saveOrUpdateStep();
                viewModel.setCurrentIdQuestStep(0);
                QuestStepFragment questStepFragment = new QuestStepFragment();
                questStepFragment.setFragmentManager(fragmentManager);
                fragmentManager.beginTransaction()
                        .remove(this)
                        .addToBackStack(null)
                        .add(R.id.fragment_container, questStepFragment)
                        .commit();
                break;
        }
    }

    private void downloadFile(int caseCode) {
        if (TextUtils.isEmpty(titleStep.getText().toString()) || TextUtils.isEmpty(conditionForNextStep.getText().toString())) {
            Toast.makeText(getContext(), "Проверьте наличие названия шага и условия перехода к следующему шагу.", Toast.LENGTH_SHORT).show();
        } else {
            viewModel.saveOrUpdateQuestStep(titleStep.getText().toString(), conditionForNextStep.getText().toString());
            switch (caseCode) {
                case -1:
                    Toast.makeText(getContext(), "Ничего не выбрано", Toast.LENGTH_SHORT).show();
                    break;
                case 0:
                    RecordAudioFragment recordAudioFragment = new RecordAudioFragment();
                    recordAudioFragment.setFragmentManager(fragmentManager);
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, recordAudioFragment)
                            .addToBackStack(null)
                            .commit();
                    break;
                case 1:
                    Intent takeVideoIntent = mediaManager.getPickVideoIntent();
                    startActivityForResult(takeVideoIntent, TAKE_VIDEO);
                    break;
                case 2:
                    Intent takePictureIntent = mediaManager.getPickImageIntent();
                    startActivityForResult(takePictureIntent, TAKE_PICTURE);
                    break;
                case 3:
                    EnterTextFragment enterTextFragment = new EnterTextFragment();
                    enterTextFragment.setFragmentManager(fragmentManager);
                    fragmentManager.beginTransaction()
                            .replace(R.id.fragment_container, enterTextFragment)
                            .addToBackStack(null)
                            .commit();
                    break;
            }
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mDisposable.dispose();
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case TAKE_PICTURE:
                try {
                    if (resultCode == RESULT_OK) {
                        if (intent.getData() == null) {

                            String currentPhotoFilePath = viewModel.getCurrentPhotoPath();
                            File photoFile = new File(currentPhotoFilePath);
                            Uri photoUri = Uri.fromFile(photoFile);
                            bitmap = MediaStore.Images.Media.getBitmap(Objects.requireNonNull(getContext()).getContentResolver(), photoUri);
                            if (bitmap != null) {
                                bitmap = mediaManager.getResizedBitmap(bitmap, 120);
                            }
                            mDisposable.add(viewModel.setStepPosition(2, "photo", currentPhotoFilePath)
                                    .subscribe(() -> {
                                            },
                                            throwable -> Log.e(TAG, "Unable to save step position")
                                    ));
                        } else {
                            try {
                                Uri imageUri = intent.getData();
                                bitmap = MediaStore.Images.Media.getBitmap(Objects.requireNonNull(getContext()).getContentResolver(), imageUri);
                                String currentPhotoFilePath = mediaManager.getRealPathFromURIImage(imageUri);
                                if (bitmap != null) {
                                    bitmap = mediaManager.getResizedBitmap(bitmap, 120);
                                }
                                mDisposable.add(viewModel.setStepPosition(2, "photo", currentPhotoFilePath)
                                        .subscribe(() -> {
                                                },
                                                throwable -> Log.e(TAG, "Unable to save step position")
                                        ));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        mediaManager.galleryAddPic();
                        imageView.setImageBitmap(bitmap);
                    } else { // Result was a failure
                        Toast.makeText(getContext(), "Picture wasn't taken!", Toast.LENGTH_SHORT).show();
                    }
                    break;
                } catch (Exception error) {
                    error.printStackTrace();
                }


                break;
            case TAKE_VIDEO:
                try {
                    if (resultCode == RESULT_OK) {
                        if (intent.getData() == null) {
                            String currentVideoFilePath = viewModel.getCurrentVideoPath();
                            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(currentVideoFilePath, MediaStore.Video.Thumbnails.MICRO_KIND);
                            mDisposable.add(viewModel.setStepPosition(1, "video", currentVideoFilePath)
                                    .subscribe(() -> {
                                            },
                                            throwable -> Log.e(TAG, "Unable to save step position")
                                    ));
                            imageView.setImageBitmap(bitmap);
                        } else {
                            try {
                                Uri videoUri = intent.getData();
                                String currentVideoFilePath = MediaManager.getFilePathFromURI(getContext(), videoUri);
                                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(currentVideoFilePath, MediaStore.Video.Thumbnails.MICRO_KIND);

                                mDisposable.add(viewModel.setStepPosition(1, "video", currentVideoFilePath)
                                        .subscribe(() -> {
                                                },
                                                throwable -> Log.e(TAG, "Unable to save step position")
                                        ));
                                imageView.setImageBitmap(bitmap);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else { // Result was a failure
                        Toast.makeText(getContext(), "Video wasn't taken!", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception error) {
                    error.printStackTrace();
                }
                break;
        }
    }
}

