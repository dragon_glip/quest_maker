package com.example.glip.quest_00.view;

import com.example.glip.quest_00.db.entity.StepsEntity;

public class ChoseStepFragmentPlay extends ChoseStepFragment {

    @Override
    public void onStepClicked(StepsEntity stepsEntity) {
        viewModel.setPreviousStepId(stepsEntity.getStepId());
        fragmentManager.popBackStackImmediate();
    }

    @Override
    public void onDeleteStepClicked(StepsEntity stepsEntity) {

    }
}
