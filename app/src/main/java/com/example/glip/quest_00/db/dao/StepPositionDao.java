package com.example.glip.quest_00.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.glip.quest_00.db.entity.StepPositionEntity;


import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface StepPositionDao {
    @Insert
    long insert (StepPositionEntity stepPositionEntity);

    @Delete
    void delete(StepPositionEntity stepPositionEntity);

    @Query("SELECT * FROM step_position WHERE step_id = :step_id ORDER BY position")
    List<StepPositionEntity> getById(long step_id);

    @Query("SELECT * FROM step_position")
    List<StepPositionEntity> getAll();

    @Query("DELETE FROM step_position WHERE step_id = :step_id")
    abstract void deleteById(long step_id);

    @Query("UPDATE step_position SET position = position - 1 WHERE position > :currentPosition")
    void rebuildPosition(int currentPosition);


}
