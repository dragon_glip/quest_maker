package com.example.glip.quest_00.view;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;


public class QuestCreateFragment extends Fragment {
    private Unbinder unbinder;
    View rootView;
    @BindView(R.id.qwest_name)
    EditText questName;
    @BindView(R.id.happy_birthday)
    RadioButton happyBirthday;
    @BindView(R.id.halloween)
    RadioButton halloween;
    @BindView(R.id.pirate)
    RadioButton pirate;
    @BindView(R.id.create_qwest)
    Button create_quest;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.create_quest_layout)
    ConstraintLayout constraintLayout;
    int themeCode;
    int THEME_HAPPY_BIRTHDAY = 0;
    int THEME_HALLOWEEN = 1;
    int THEME_PIRATE = 2;
    //DataBaseHelper db;
    FragmentManager fragmentManager;
    private QuestViewModel viewModel;
    private final CompositeDisposable mDisposable = new CompositeDisposable();

    public QuestCreateFragment() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.create_quest, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(QuestViewModel.class);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        create_quest.setOnClickListener(v -> saveNewQuest());

        //db = App.getInstance().getDatabaseInstance();
        radioGroup.setOnCheckedChangeListener((radioGroup, checkedId) -> {
            switch (checkedId) {
                case -1:
                    Toast.makeText(getContext(), "Ничего не выбрано",
                            Toast.LENGTH_SHORT).show();
                    break;
                case R.id.happy_birthday:
                    constraintLayout.setBackgroundResource(R.drawable.backgroundhb);
                    themeCode = THEME_HAPPY_BIRTHDAY;

                    break;
                case R.id.halloween:
                    constraintLayout.setBackgroundResource(R.drawable.halloween);
                    themeCode = THEME_HALLOWEEN;
                    break;
                case R.id.pirate:
                    constraintLayout.setBackgroundResource(R.drawable.pirate);
                    themeCode = THEME_PIRATE;
                    break;

                default:
                    break;
            }
        });
    }


    public static Fragment newInstans() {
        return new QuestCreateFragment();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDisposable.dispose();
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    private void saveNewQuest() {
        String questTitle = questName.getText().toString();
        Date questCreated = new Date(System.currentTimeMillis());
        //create_quest.setEnabled(false);

        viewModel.saveQuest(1, questTitle, themeCode, questCreated, questCreated);
        transactionFragment();
//        mDisposable.add(viewModel.saveQuest(1, questTitle, themeCode, questCreated, questCreated)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(this::transactionFragment,
//                        throwable -> Log.e(TAG, "Unable to save quest", throwable)));
    }

    private void transactionFragment() {
        viewModel.setCurrentIdQuestStep(0);
        QuestStepFragment questStepFragment = new QuestStepFragment();
        questStepFragment.setFragmentManager(fragmentManager);
        fragmentManager.beginTransaction()
                .remove(this)
                .add(R.id.fragment_container, questStepFragment)
                .addToBackStack(null)
                .commit();
    }
}
