package com.example.glip.quest_00.view;


import com.example.glip.quest_00.R;
import com.example.glip.quest_00.db.entity.StepsEntity;

public class ChoseStepFragmentCreator extends ChoseStepFragment {


    @Override
    public void onStepClicked(StepsEntity stepsEntity) {
        viewModel.setCurrentIdQuestStep(stepsEntity.getStepId());
        QuestStepFragment questStepFragment = new QuestStepFragment();
        questStepFragment.setFragmentManager(fragmentManager);
        fragmentManager.beginTransaction()
                .remove(this)
                .add(R.id.fragment_container, questStepFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDeleteStepClicked(StepsEntity stepsEntity) {
        createDialog = new MyDeleteDialog();
        createDialog.createDialog(getContext(), stepsEntity, viewModel);
        initializationRecyclerView();
    }
}
