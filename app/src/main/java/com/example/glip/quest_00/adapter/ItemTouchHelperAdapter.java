package com.example.glip.quest_00.adapter;

public interface ItemTouchHelperAdapter {

    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
    void onItemRestore(int deletedPosition);

}
