package com.example.glip.quest_00.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.example.glip.quest_00.db.entity.QuestsEntity;
import com.example.glip.quest_00.db.entity.StepPositionEntity;
import com.example.glip.quest_00.db.entity.StepsEntity;
import com.example.glip.quest_00.viewmodel.QuestViewModel;


class MyDeleteDialog {

    void createDialog(Context context, StepPositionEntity stepPositionEntity, QuestViewModel viewModel) {
        String title = "Вы уверены?";
        String button1String = "OK";
        String button2String = "Сancel";

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);  // заголовок

        builder.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                viewModel.deleteStepPosition(stepPositionEntity);

            }
        });
        builder.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(context, "Возможно вы правы", Toast.LENGTH_LONG)
                        .show();
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    void createDialog(Context context, StepsEntity stepsEntity, QuestViewModel viewModel) {
        String title = "Вы уверены?";
        String button1String = "OK";
        String button2String = "Сancel";

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);  // заголовок

        builder.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                viewModel.deleteStep(stepsEntity);
                viewModel.deleteAllStepPosition(stepsEntity.getStepId());
            }
        });
        builder.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(context, "Возможно вы правы", Toast.LENGTH_LONG)
                        .show();
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    void createDialog(Context context, QuestsEntity questsEntity, QuestViewModel viewModel) {
        String title = "Вы уверены?";
        String button1String = "OK";
        String button2String = "Сancel";

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);  // заголовок

        builder.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                viewModel.deleteQuest(questsEntity);

            }
        });
        builder.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(context, "Возможно вы правы", Toast.LENGTH_LONG)
                        .show();
            }
        });
        builder.setCancelable(true);
        builder.show();
    }

    //**окно отмены прохождения квеста**//
    void createDialog(Context context, FragmentManager fragmentManager) {
        String title = "Вы уверены, что хотите прекратить прохождение квеста?";
        String button1String = "OK";
        String button2String = "Сancel";

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);  // заголовок

        builder.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(context, com.example.glip.quest_00.view.FragmentContainer.class);
                context.startActivity(intent);
            }
        });
        builder.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(context, "Возможно вы правы", Toast.LENGTH_LONG)
                        .show();
            }
        });
        builder.setCancelable(false);
        AlertDialog alert = builder.create();
        alert.show();
    }

}
