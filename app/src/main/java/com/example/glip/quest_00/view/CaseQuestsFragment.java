package com.example.glip.quest_00.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.adapter.QuestItemAdapter;
import com.example.glip.quest_00.db.entity.QuestsEntity;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CaseQuestsFragment extends Fragment implements QuestItemAdapter.QuestItemAdapterListener {
    QuestItemAdapter questItemAdapter;
    @BindView(R.id.recyclerView2)
    RecyclerView recyclerView;
    @BindView(R.id.create_quest)
    Button create_quest;
    private Unbinder unbinder;
    FragmentManager fragmentManager;
    private QuestViewModel viewModel;
    View rootView;
    MyDeleteDialog createDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.case_quest_fragment, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(QuestViewModel.class);
        }
        initializationRecyclerView();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        create_quest.setOnClickListener(view -> createNewQuest());
    }

    private void initializationRecyclerView() {
        questItemAdapter = new QuestItemAdapter(this);
        questItemAdapter.submitList(viewModel.getQuestsList());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(questItemAdapter);
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void createNewQuest() {

        QuestCreateFragment questCreateFragment = new QuestCreateFragment();
        questCreateFragment.setFragmentManager(fragmentManager);
        fragmentManager.beginTransaction()
                .remove(this)
                .add(R.id.fragment_container, questCreateFragment)
                .commit();

    }

    @Override
    public void onQuestClicked(QuestsEntity questsEntity) {
        viewModel.setCurrentIdQuest(questsEntity.getId());
        ChoseStepFragmentCreator choseStepFragmentCreator = new ChoseStepFragmentCreator();
        choseStepFragmentCreator.setFragmentManager(fragmentManager);
        fragmentManager.beginTransaction()
                .remove(this)
                .add(R.id.fragment_container, choseStepFragmentCreator)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onQuestPlayClicked(QuestsEntity questsEntity) {
        // PreviewFragment previewFragment = new PreviewFragment();
        //viewModel.setCurrentIdQuest(questsEntity.getId());
        long tempQuestId = questsEntity.getId();
        long tempFirstStepId = viewModel.getStepByPrevious(0).getStepId();
        // viewModel.setCurrentIdQuestStep(viewModel.getStepByPrevious(0).getStepId());
        Intent startPlayQuestIntent = new Intent(getContext(), PlayQuestActivity.class);
        startPlayQuestIntent.putExtra("tempQuestId", tempQuestId);
        startPlayQuestIntent.putExtra("tempFirstStepId", tempFirstStepId);
        startActivity(startPlayQuestIntent);

    }

    @Override
    public void onQuestDeleteClicked(QuestsEntity questsEntity) {
        createDialog = new MyDeleteDialog();
        createDialog.createDialog(getContext(), questsEntity, viewModel); //создаем алерт диалог для подтверждения удаления из базы
        initializationRecyclerView();
    }
}
