package com.example.glip.quest_00.model;

import android.net.Uri;

import java.net.URI;

public interface StepPosition {
    int getPosition();
    String getText();
    String getCurrentFilePath();
    long getStepId();
}
