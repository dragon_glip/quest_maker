package com.example.glip.quest_00.view;

import android.arch.lifecycle.ViewModelProviders;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PlayAudioFragment extends Fragment {
    private int lastProgress;
    private Handler mHandler;
    private boolean isPlaying;
    private MediaPlayer mPlayer;
    @BindView(R.id.imageViewPlay)
    ImageView imageViewPlay;
    @BindView(R.id.seekBar)
    SeekBar seekBar;
    View rootView;

    private Unbinder unbinder;
    FragmentManager fragmentManager;
    @BindView(R.id.mCurrentProgressTextView)
    TextView mCurrentProgressTextView;
    private QuestViewModel viewModel;
    private File mAudioFile;
    private String currentAudioFilePath;

    public PlayAudioFragment() {
        mHandler = new Handler();
        mAudioFile = null;
        isPlaying = false;
        currentAudioFilePath = null;
    }

    @OnClick({R.id.imageViewPlay, R.id.mCurrentProgressTextView})
    void onSaveClick(View view) {
        switch (view.getId()) {

            case R.id.imageViewPlay:
                if (!isPlaying && mAudioFile != null) {
                    isPlaying = true;

                    startPlaying();
                } else {
                    isPlaying = false;
                    stopPlaying();
                }
                break;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(QuestViewModel.class);
        }
        if (currentAudioFilePath == null) {
            mAudioFile = viewModel.getAudioFile();
        } else {
            mAudioFile = new File(currentAudioFilePath);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.play_audio_fragment, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void startPlaying() {
        mPlayer = new MediaPlayer();
        Log.d("instartPlaying", mAudioFile.getName());
        try {
            mPlayer.setDataSource(mAudioFile.getPath());
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e("LOG_TAG", "prepare() failed");
        }
        //making the imageview pause button
        imageViewPlay.setImageResource(R.drawable.ic_pause);

        seekBar.setProgress(lastProgress);
        mPlayer.seekTo(lastProgress);
        seekBar.setMax(mPlayer.getDuration());
        seekUpdation();
        long minutes = TimeUnit.MILLISECONDS.toMinutes(mPlayer.getCurrentPosition());
        long seconds = TimeUnit.MILLISECONDS.toSeconds(mPlayer.getCurrentPosition())
                - TimeUnit.MINUTES.toSeconds(minutes);
        mCurrentProgressTextView.setText(String.format("%02d:%02d", minutes, seconds));


        /** once the audio is complete, timer is stopped here**/
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                imageViewPlay.setImageResource(R.drawable.ic_play);
                isPlaying = false;
                lastProgress = 0;
                mPlayer.stop();

            }
        });

        /** moving the track as per the seekBar's position**/
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mPlayer != null && fromUser) {
                    //here the track's progress is being changed as per the progress bar
                    mPlayer.seekTo(progress);
                    //timer is being updated as per the progress of the seekbar
                    mHandler.removeCallbacks(runnable);
                    long minutes = TimeUnit.MILLISECONDS.toMinutes(mPlayer.getCurrentPosition());
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(mPlayer.getCurrentPosition())
                            - TimeUnit.MINUTES.toSeconds(minutes);
                    mCurrentProgressTextView.setText(String.format("%02d:%02d", minutes, seconds));
                    lastProgress = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void stopPlaying() {
        mHandler.removeCallbacks(runnable);
        try {
            mPlayer.stop();
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        isPlaying = !isPlaying;
        //showing the play button
        imageViewPlay.setImageResource(R.drawable.ic_play);
        seekBar.setProgress(seekBar.getMax());
        mCurrentProgressTextView.setText((Long.toString(mAudioFile.length())));
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (mPlayer != null) {

                int mCurrentPosition = mPlayer.getCurrentPosition();
                seekBar.setProgress(mCurrentPosition);
                lastProgress = mCurrentPosition;
                long minutes = TimeUnit.MILLISECONDS.toMinutes(mCurrentPosition);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(mCurrentPosition)
                        - TimeUnit.MINUTES.toSeconds(minutes);
                mCurrentProgressTextView.setText(String.format("%02d:%02d", minutes, seconds));

                seekUpdation();
            }
        }
    };

    private void seekUpdation() {
        mHandler.postDelayed(runnable, 1000);
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }


    public void setCurrentAudioFilePath(String currentAudioFilePath) {
        this.currentAudioFilePath = currentAudioFilePath;
    }
}
