package com.example.glip.quest_00.helper;

import android.support.v7.widget.RecyclerView;

public interface OnStartDragListener {
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
