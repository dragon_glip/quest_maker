package com.example.glip.quest_00.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import com.example.glip.quest_00.db.entity.StepsEntity;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface StepsDao {
    @Insert
    long insert(StepsEntity stepsEntity);

    @Update
    void update(StepsEntity stepsEntity);

    @Delete
    void delete(StepsEntity stepsEntity);

    @Query("SELECT * FROM steps")
    List<StepsEntity> getAllData();

    //пример запроса с выборкой
    @Query("SELECT * FROM steps WHERE quest_id LIKE :quest_id")
    List<StepsEntity> getByQuestId(long quest_id);

    @Query("SELECT * FROM steps WHERE step_id = :id")
    StepsEntity getById(long id);

    @Query("SELECT * FROM steps WHERE previousStepId = :id")
    StepsEntity getByPreviousStep(long id);

    @Query("SELECT * FROM steps WHERE step_id = :id")
    Flowable<StepsEntity> getByIdFlow(long id);

    @Query("UPDATE steps SET title = :title WHERE step_id = :id" )
    void updateStepTitle (String title, long id);
}
