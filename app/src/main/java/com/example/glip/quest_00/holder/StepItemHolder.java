package com.example.glip.quest_00.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import com.example.glip.quest_00.databinding.StepItemCardBinding;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.glip.quest_00.R;

public class StepItemHolder extends  RecyclerView.ViewHolder {

    public final StepItemCardBinding binding;

    public StepItemHolder(final StepItemCardBinding itemBinding) {
        super(itemBinding.getRoot());
        this.binding = itemBinding;
    }
}
