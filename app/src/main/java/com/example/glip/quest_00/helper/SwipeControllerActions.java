package com.example.glip.quest_00.helper;

public abstract class SwipeControllerActions {
    public void onLeftClicked(int position) {}

    public void onRightClicked(int position) {}
}
