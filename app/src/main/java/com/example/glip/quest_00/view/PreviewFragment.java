package com.example.glip.quest_00.view;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.db.entity.StepPositionEntity;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

import java.io.File;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

public class PreviewFragment extends Fragment {
    @BindView(R.id.title_quest)
    TextView titleQuest;
    @BindView(R.id.title_step)
    TextView titleStep;
    @BindView(R.id.linearLayout_constructor)
    LinearLayout linearLayoutConstructor;
    View rootView;
    FragmentManager fragmentManager;
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private Unbinder unbinder;
    private QuestViewModel viewModel;
    List<StepPositionEntity> stepPositionEntities;
    boolean previewOrPlay;
    EditText editTextConditionForNextStep;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stepPositionEntities = null;
        previewOrPlay = false;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.preview_fragment, container, false);
        unbinder = ButterKnife.bind(this, rootView);


        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(QuestViewModel.class);
        }
        chooseBackgraund(viewModel.getTheme());
        stepPositionEntities = viewModel.getStepPositionList();
        mDisposable.add(viewModel.getQuestNameFlow()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(questTitle -> titleQuest.setText(questTitle),
                        throwable -> Log.e(TAG, "Unable to update username", throwable)));
        mDisposable.add(viewModel.getStepNameFlow()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stepTitle -> titleStep.setText(stepTitle),
                        throwable -> Log.e(TAG, "Unable to update username", throwable)));
        if (stepPositionEntities != null) {
            for (int i = 0; i < stepPositionEntities.size(); i++) {
                try {
                    constructor(stepPositionEntities.get(i));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (!previewOrPlay) {
            editTextConditionForNextStep = new EditText(getContext());
            editTextConditionForNextStep.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            linearLayoutConstructor.addView(editTextConditionForNextStep);
        } else {
            editTextConditionForNextStep = new EditText(getContext());
            editTextConditionForNextStep.setText(viewModel.getCondition());
            editTextConditionForNextStep.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            linearLayoutConstructor.addView(editTextConditionForNextStep);
        }


        if (!previewOrPlay) {
            Button ok = new Button(getContext());
            ok.setTextColor(getResources().getColor(R.color.colorPrimary));
            ok.setText(R.string.next_step_button);
            ok.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    String conditionForNextStep = viewModel.getCondition();

                    if (editTextConditionForNextStep.getText().toString().compareToIgnoreCase(conditionForNextStep) != 0) {
                        Toast.makeText(getContext(), "Введите верное значение для перехода к следующему шагу", Toast.LENGTH_SHORT)
                                .show();
                        return;
                    }
                    nextStep();
                }
            });
            linearLayoutConstructor.addView(ok);
        }


        return rootView;
    }

    private void nextStep() {
        PreviewFragment previewFragment = new PreviewFragment();
        previewFragment.setFragmentManager(fragmentManager);
        if (viewModel.getNextStep()) {
            fragmentManager.beginTransaction()
                    .remove(this)
                    .replace(R.id.fragment_container, previewFragment)
                    .commit();
        } else {
            //something after finish
        }
    }

    private void chooseBackgraund(int themeCode) {
        switch (themeCode) {
            case 0:
                linearLayoutConstructor.setBackgroundResource(R.drawable.backgroundhb);
                break;
            case 1:
                linearLayoutConstructor.setBackgroundResource(R.drawable.halloween);
                break;
            case 2:
                linearLayoutConstructor.setBackgroundResource(R.drawable.pirate);
                break;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void constructor(StepPositionEntity stepPositionEntity) {
        int type = stepPositionEntity.getType();
        switch (type) {
            case 0:
                FrameLayout frameLayout = new FrameLayout(Objects.requireNonNull(getContext()));
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                frameLayout.setId(R.id.conteyner_for_audio);
                linearLayoutConstructor.addView(frameLayout);
                String currentAudioFilePath = stepPositionEntity.getCurrentFilePath();
                PlayAudioFragment playAudioFragment = new PlayAudioFragment();
                playAudioFragment.setCurrentAudioFilePath(currentAudioFilePath);
                playAudioFragment.setFragmentManager(fragmentManager);
                fragmentManager.beginTransaction()
                        .add(R.id.conteyner_for_audio, playAudioFragment)
                        .commit();
                break;
            case 1:
                VideoView videoView = new VideoView(getContext());
                MediaController mediaController = new MediaController(getContext());
                mediaController.setAnchorView(videoView);
                File videoFile = new File(stepPositionEntity.getCurrentFilePath());
                Uri videoUri = Uri.fromFile(videoFile);
                try {
                    videoView.setMediaController(mediaController);
                    videoView.setVideoURI(videoUri);
                    videoView.setLayoutParams(new LinearLayout.LayoutParams(500, 500));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                videoView.requestFocus();
                //videoView.start();
                linearLayoutConstructor.addView(videoView);
                break;
            case 2:
                ImageView imageView = new ImageView(getContext());
                imageView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                File photoFile = new File(stepPositionEntity.getCurrentFilePath());
                Uri photoUri = Uri.fromFile(photoFile);
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(Objects.requireNonNull(getContext()).getContentResolver(), photoUri);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                imageView.setImageBitmap(bitmap);
                linearLayoutConstructor.addView(imageView);
                break;
            case 3:
                TextView textView = new TextView(getContext());
                textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                textView.setTextSize(19);
                textView.setTextColor(Color.parseColor("#742500"));
                textView.setText(stepPositionEntity.getText());
                linearLayoutConstructor.addView(textView);
                break;
        }
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setPreview(boolean b) {
        this.previewOrPlay = b;
    }
}
