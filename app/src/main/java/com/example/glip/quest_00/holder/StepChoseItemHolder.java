package com.example.glip.quest_00.holder;

import android.support.v7.widget.RecyclerView;

import com.example.glip.quest_00.databinding.ChoseStepItemBinding;
import com.example.glip.quest_00.helper.ItemTouchHelperViewHolder;

public class StepChoseItemHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
    public final ChoseStepItemBinding binding;

    public StepChoseItemHolder(final ChoseStepItemBinding itemBinding) {
        super(itemBinding.getRoot());
        this.binding = itemBinding;
    }

    @Override
    public void onItemSelected() {

    }

    @Override
    public void onItemClear() {

    }
}
