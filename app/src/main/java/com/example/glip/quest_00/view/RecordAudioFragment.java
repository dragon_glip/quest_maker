package com.example.glip.quest_00.view;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.transition.TransitionManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.utils.MediaManager;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

import static android.content.ContentValues.TAG;

public class RecordAudioFragment extends Fragment implements View.OnClickListener {
    private int RECORD_AUDIO_REQUEST_CODE = 123;
    View rootView;
    private Unbinder unbinder;
    @BindView(R.id.imageViewRecord)
    ImageView imageViewRecord;
    @BindView(R.id.imageViewStop)
    ImageView imageViewStop;
    private QuestViewModel viewModel;
    PlayAudioFragment playAudioFragment;
    @BindView(R.id.save_button)
    Button save_button;
    @BindView(R.id.linearLayoutRecorder)
    LinearLayout linearLayoutRecorder;
    protected MediaRecorder mRecorder;
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private File mAudioFile;
    MediaManager mediaManager;
    @BindView(R.id.mChronometer)
    Chronometer mChronometer;

    FragmentManager fragmentManager;

    public RecordAudioFragment() {
        mAudioFile = null;
        mediaManager = new MediaManager(getContext());
        playAudioFragment = new PlayAudioFragment();
        playAudioFragment.setFragmentManager(fragmentManager);


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(QuestViewModel.class);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getPermissionToRecordAudio();
        }

    }

    @OnClick({R.id.imageViewRecord, R.id.imageViewStop, R.id.save_button})
    void onSaveClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewRecord:
                prepareforRecording();
                startRecording();
                break;
            case R.id.imageViewStop:
                prepareforStop();
                stopRecording();
                break;
            case R.id.save_button:
                onSavePosition();
                break;

        }
    }

    private void onSavePosition() {
        String currentAudioFilePath = mAudioFile.getAbsolutePath();
        viewModel.setCurrentAudioFilePath(currentAudioFilePath);
        mDisposable.add(viewModel.setStepPosition(0, "your audio", currentAudioFilePath)
                .subscribe(() -> {
                        },
                        throwable -> {
                            Log.e(TAG, "Unable to save step position");
                        }
                ));
    }

    private void prepareforRecording() {
        TransitionManager.beginDelayedTransition(linearLayoutRecorder);
        imageViewRecord.setVisibility(View.GONE);
        imageViewStop.setVisibility(View.VISIBLE);

    }

    private void startRecording() {
        if (playAudioFragment.isVisible()) {
            fragmentManager.beginTransaction()
                    .remove(playAudioFragment)
                    .commit();

        }
        //we use the MediaRecorder class to record
        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        /**In the lines below, we create a directory named VoiceRecorderSimplifiedCoding/Audios in the phone storage
         * and the audios are being stored in the Audios folder **/
        File root = Environment.getExternalStorageDirectory();
//        File file = new File(root.getAbsolutePath() + "/VoiceRecorderSimplifiedCoding/Audios");
//        if (!file.exists()) {
//            file.mkdirs();
//        }

        mAudioFile = mediaManager.createAudioFile();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mRecorder.setOutputFile(mAudioFile);
        }

        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mChronometer.setBase(SystemClock.elapsedRealtime());
        mChronometer.start();
    }

    private void stopRecording() {
        try {
            mRecorder.stop();
            mRecorder.release();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mRecorder = null;
        //starting the chronometer
        mChronometer.stop();
        mChronometer.setBase(SystemClock.elapsedRealtime());
        //showing the play button
        Toast.makeText(getContext(), "Recording saved successfully.", Toast.LENGTH_SHORT).show();

    }

    private void prepareforStop() {
        TransitionManager.beginDelayedTransition(linearLayoutRecorder);
        imageViewRecord.setVisibility(View.VISIBLE);
        imageViewStop.setVisibility(View.GONE);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.record_frame_layout, playAudioFragment);
        transaction.commit();

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.record_audio, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onClick(View v) {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getPermissionToRecordAudio() {
        // 1) Use the support library version ContextCompat.checkSelfPermission(...) to avoid
        // checking the build version since Context.checkSelfPermission(...) is only available
        // in Marshmallow
        // 2) Always check for permission (even if permission has already been granted)
        // since the user can revoke permissions at any time through Settings
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // The permission is NOT already granted.
            // Check if the user has been asked about this permission already and denied
            // it. If so, we want to give more explanation about why the permission is needed.
            // Fire off an async request to actually get the permission
            // This will show the standard permission request dialog UI
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    RECORD_AUDIO_REQUEST_CODE);

        }
    }

    // Callback with the request from calling requestPermissions(...)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == RECORD_AUDIO_REQUEST_CODE) {
            if (grantResults.length == 3 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                //Toast.makeText(this, "Record Audio permission granted", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(getContext(), "You must give permissions to use this app. App is exiting.", Toast.LENGTH_SHORT).show();
                //finishAffinity();
            }
        }

    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }
}
