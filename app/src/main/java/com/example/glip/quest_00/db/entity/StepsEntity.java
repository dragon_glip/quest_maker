package com.example.glip.quest_00.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import com.example.glip.quest_00.BR;
import com.example.glip.quest_00.model.Steps;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        tableName = "steps",
        foreignKeys = @ForeignKey(entity = QuestsEntity.class, parentColumns = "id", childColumns = "quest_id", onDelete = CASCADE),
        indices = {@Index(value = "quest_id")
})

public class StepsEntity extends BaseObservable implements Steps {

    @ColumnInfo(name = "step_id")
    @PrimaryKey(autoGenerate = true)
    private int stepId;

    @ColumnInfo(name = "quest_id")
    private long questId;
    private int previousStepId;
    private Date dateCreated;
    private Date dateModified;
    private String condition;
    private String title;


    public StepsEntity (long questId, String title, String condition, Date dateCreated, Date dateModified, int previousStepId){
        this.questId = questId;
        this.dateCreated = dateCreated;
        this.dateModified = dateModified;
        this.title = title;
        this.condition = condition;
        this.previousStepId = previousStepId;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public int getStepId() {
        return stepId;
    }

    public void setStepId(int id) {
        this.stepId = id;
    }

    public long getQuestId() {
        return questId;
    }

    public void setQuestId(int questId) {
        this.questId = questId;
    }

    public void setPreviousStepId(int previousStepId) {
        this.previousStepId = previousStepId;
    }

    public int getPreviousStepId() {
        return previousStepId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
