package com.example.glip.quest_00.model;

public interface Users {
    int getId();
    String getName();
    String getPassword();
}
