package com.example.glip.quest_00.db.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;

import com.example.glip.quest_00.BR;

import com.example.glip.quest_00.model.StepPosition;

import java.net.URI;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(
        tableName = "step_position"
//        foreignKeys = @ForeignKey(entity = StepsEntity.class, parentColumns = "id", childColumns = "step_id", onDelete = CASCADE),
//        indices = {@Index(value = "step_id")
//        }
        )
public class StepPositionEntity extends BaseObservable implements StepPosition {

    @ColumnInfo(name = "step_position_id")
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "step_id")
    private long stepId;
    private int type; //0-audio file; 1 - video file; 2 - image file; 3 - text file;
    private int position;
    private String text;
    private String currentFilePath;


    public StepPositionEntity(long stepId, int position, int type, String text, String currentFilePath ) {
        this.stepId = stepId;
        this.position = position;
        this.type = type;
        this.text = text;
        this.currentFilePath = currentFilePath;
    }
    @Override
    public int getPosition() {
        return position;
    }

    @Override
    @Bindable
    public String getText() {
        return text;
    }

    public long getStepId() {
        return stepId;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setStepId(long stepId) {
        this.stepId = stepId;
    }

    public void setText(String text) {

        this.text = text;
        notifyPropertyChanged(BR.text);
    }

    public void setCurrentFilePath(String currentFilePath) {
        this.currentFilePath = currentFilePath;
    }

    public String getCurrentFilePath() {
        return currentFilePath;
    }

}
