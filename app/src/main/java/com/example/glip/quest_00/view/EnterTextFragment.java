package com.example.glip.quest_00.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.glip.quest_00.R;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

public class EnterTextFragment extends Fragment {
    View rootView;
    @BindView(R.id.textInputQuestion)
    EditText textInputQuestion;
    @BindView(R.id.ok)
    Button buttonOk;
    FragmentManager fragmentManager;
    private Unbinder unbinder;
    private QuestViewModel viewModel;
    private final CompositeDisposable mDisposable = new CompositeDisposable();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.enter_text_fragment, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(QuestViewModel.class);
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {


        buttonOk.setOnClickListener(v ->

                mDisposable.add(viewModel.setStepPosition(3, textInputQuestion.getText().toString(), null)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(this::returnInStepFragment,
                                throwable -> Log.e(TAG, "Unable to save quest", throwable)))
        );
        super.onActivityCreated(savedInstanceState);
    }

    private void returnInStepFragment() {

        fragmentManager.popBackStack();

    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        mDisposable.dispose();
        super.onDestroy();
    }
}
