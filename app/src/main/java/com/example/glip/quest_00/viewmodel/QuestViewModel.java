package com.example.glip.quest_00.viewmodel;


import android.arch.lifecycle.ViewModel;

import com.example.glip.quest_00.App;
import com.example.glip.quest_00.db.DataBaseHelper;
import com.example.glip.quest_00.db.entity.QuestsEntity;
import com.example.glip.quest_00.db.entity.StepPositionEntity;
import com.example.glip.quest_00.db.entity.StepsEntity;

import java.io.File;
import java.util.Date;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;


public class QuestViewModel extends ViewModel {

    private DataBaseHelper db;
    private QuestsEntity mQuest;
    private StepsEntity stepsEntity;
    private QuestsEntity questEntity;
    private StepPositionEntity mStepPosition;
    private long currentIdQuest;
    public long currentIdQuestStep;
    private String currentQuestion = null;
    private long currentIdStepPosition;
    private File mAudioFile;

    private List<StepPositionEntity> currentStepPositionEntity;
    private String currentPhotoFilePath;
    private String currentVideoFilePath;
    private String currentAudioFilePath;

    public QuestViewModel() {
        db = App.getInstance().getDatabaseInstance();
        currentIdQuest = 0;
        currentIdQuestStep = 0;
    }

    public Flowable<String> getUserName() {
        return db.getQuestsDao().getByTitle("qw1")
                // for every emission of the user, get the user name
                .map(quest -> {
                    mQuest = quest;
                    return quest.getTitle();
                });
    }

    public void saveQuest(final int userId, final String titleQuest, final int themeQuest,
                          final Date dateCreatedQuest, final Date dateModifiedQuest) {
        mQuest = new QuestsEntity(userId, titleQuest, themeQuest, dateCreatedQuest, dateModifiedQuest);
        long id = db.getQuestsDao().insertQuest(mQuest);
        setCurrentIdQuest(id);

    }

    public String getCondition() {
        return db.getStepsDao().getById(currentIdQuestStep).getCondition();
    }

    public StepsEntity getStepByPrevious(long id) {
        return db.getStepsDao().getByPreviousStep(id);
    }

    public Completable setStepPosition(final int type, String text, String uriFile) {
        return Completable.fromAction(() -> {
            int position = getStepPositionList().size() + 1;
            mStepPosition = new StepPositionEntity(currentIdQuestStep, position, type, text, uriFile);
            currentIdStepPosition = db.getStepPositionDao().insert(mStepPosition);

        });
    }

    public boolean getNextStep() {
        long currentStepId = getStep().getStepId();
        if (getStepByPrevious(currentStepId) != null) {
            setCurrentIdQuestStep(getStepByPrevious(currentStepId).getStepId());
            return true;
        } else {
            return false;

        }

    }

    public List<StepPositionEntity> getStepPositionList() {
        return db.getStepPositionDao().getById(currentIdQuestStep);
    }

    public List<QuestsEntity> getQuestsList() {
        return db.getQuestsDao().getAllData();
    }


    public List<StepsEntity> getStepListByQuestId() {

        return db.getStepsDao().getByQuestId(currentIdQuest);
    }


    public void saveOrUpdateQuestStep(String stepTitle, String stepCondition) {
        if (currentIdQuestStep > 0) {
            StepsEntity stepsEntity = db.getStepsDao().getById(currentIdQuestStep);
            stepsEntity.setTitle(stepTitle);
            stepsEntity.setCondition(stepCondition);
            db.getStepsDao().update(stepsEntity);
            //db.getStepsDao().updateStepTitle(stepTitle, currentIdQuestStep);
        } else {
            StepsEntity stepsEntity = new StepsEntity(currentIdQuest, stepTitle, stepCondition, null, null, 0);
            setCurrentIdQuestStep(db.getStepsDao().insert(stepsEntity));
        }

    }

    public void setCurrentIdQuestStep(long idQuestStep) {

        this.currentIdQuestStep = idQuestStep;
    }

    public void setCurrentIdQuest(long idQuest) {
        this.currentIdQuest = idQuest;
    }

    public void updateQuestStep(long currentIdQuestStep, String stepTitle) {

        StepsEntity stepsEntity = db.getStepsDao().getById(currentIdQuestStep);
        stepsEntity.setTitle(stepTitle);
        db.getStepsDao().update(stepsEntity);
    }

    public Flowable<String> getStepNameFlow() {
        return db.getStepsDao().getByIdFlow(currentIdQuestStep)
                .map(step -> {
                    stepsEntity = step;
                    return step.getTitle();
                });
    }

    public Flowable<String> getQuestNameFlow() {
        return db.getQuestsDao().getByIdFlow(currentIdQuest)
                .map(quest -> {
                    questEntity = quest;
                    return quest.getTitle();
                });
    }

    public Flowable<String> getStepConditionFlow() {
        return db.getStepsDao().getByIdFlow(currentIdQuestStep)
                .map(step -> {
                    stepsEntity = step;
                    return step.getCondition();
                });
    }

    public String getQuestName() {
        QuestsEntity questsEntity = db.getQuestsDao().getById(currentIdQuest);

        return questsEntity.getTitle();
    }


    public StepsEntity getStep() {
        return db.getStepsDao().getById(currentIdQuestStep);
    }

    public void deleteStepPosition(StepPositionEntity stepPositionEntity) {
        db.getStepPositionDao().delete(stepPositionEntity);
        db.getStepPositionDao().rebuildPosition(stepPositionEntity.getPosition());
    }

    public void deleteStep(StepsEntity stepsEntity) {
        db.getStepsDao().delete(stepsEntity);
    }

    public void deleteAllStepPosition(int stepId) {
        db.getStepPositionDao().deleteById(stepId);
    }


    private void setCurrentAudioFile(File mAudioFile) {
        this.mAudioFile = mAudioFile;
    }

    public int getTheme() {
        return db.getQuestsDao().getById(currentIdQuest).getTheme();
    }

    public File getAudioFile() {
        return mAudioFile;
    }

    public void setCurrentPhotoFilePath(String absolutePath) {
        this.currentPhotoFilePath = absolutePath;
    }

    public void setCurrentVideoFilePath(String absolutePath) {
        this.currentVideoFilePath = absolutePath;
    }

    public void setCurrentAudioFilePath(String currentAudioFilePath) {
        this.currentAudioFilePath = currentAudioFilePath;
    }

    public String getCurrentAudioFilePath() {
        return currentAudioFilePath;
    }

    public String getCurrentPhotoPath() {
        return currentPhotoFilePath;
    }

    public String getCurrentVideoPath() {
        return currentVideoFilePath;
    }

    public void deleteQuest(QuestsEntity questsEntity) {
        db.getQuestsDao().delete(questsEntity);
    }

    public void setPreviousStepId(int stepId) {
        StepsEntity stepsEntity = db.getStepsDao().getById(currentIdQuestStep);
        stepsEntity.setPreviousStepId(stepId);
        db.getStepsDao().update(stepsEntity);
    }

    public long getCurrentIdQuestStep() {
        return currentIdQuestStep;
    }
}
