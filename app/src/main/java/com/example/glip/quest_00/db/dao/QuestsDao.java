package com.example.glip.quest_00.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.glip.quest_00.db.entity.QuestsEntity;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface QuestsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertQuest(QuestsEntity questsEntity);

    @Delete
    void delete(QuestsEntity questsEntity);

    @Query("SELECT * FROM quests")
    List<QuestsEntity> getAllData();

    //пример запроса с выборкой
    @Query("SELECT * FROM quests WHERE title LIKE :title")
    Flowable<QuestsEntity> getByTitle(String title);

    @Query("SELECT * FROM quests WHERE id = :id")
    QuestsEntity getById(long id);

    @Query("SELECT * FROM quests WHERE id = :id")
    Flowable<QuestsEntity> getByIdFlow(long id);
}
