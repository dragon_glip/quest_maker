package com.example.glip.quest_00.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import com.example.glip.quest_00.databinding.QuestItemBinding;
import android.view.View;

public class QuestItemHolder extends  RecyclerView.ViewHolder {
    public final QuestItemBinding binding;

    public QuestItemHolder(final QuestItemBinding itemBinding) {
        super(itemBinding.getRoot());
        this.binding = itemBinding;
    }
}
