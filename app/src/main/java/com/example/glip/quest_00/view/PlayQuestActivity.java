package com.example.glip.quest_00.view;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.glip.quest_00.App;
import com.example.glip.quest_00.R;
import com.example.glip.quest_00.db.DataBaseHelper;
import com.example.glip.quest_00.db.entity.UsersEntity;
import com.example.glip.quest_00.viewmodel.QuestViewModel;

public class PlayQuestActivity extends AppCompatActivity {
    FragmentManager fragmentManager;
    QuestViewModel viewModel;

    public PlayQuestActivity() {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container);
        setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        viewModel = ViewModelProviders.of(this).get(QuestViewModel.class);
        try {
            long tempQuestId = getIntent().getExtras().getLong("tempQuestId");
            long tempFirstStepId = getIntent().getExtras().getLong("tempQuestId");
            viewModel.setCurrentIdQuest(tempQuestId);
            viewModel.setCurrentIdQuestStep(tempFirstStepId);
        } catch (Exception e) {
            e.printStackTrace();
        }


        //**заглушка пока нет базы пользователей**//
        UsersEntity usersEntity = new UsersEntity();
        usersEntity.setName("admin");
        usersEntity.setPassword("admin");
        DataBaseHelper db = App.getInstance().getDatabaseInstance();
        db.getUsersDao().insert(usersEntity);


        fragmentManager = getSupportFragmentManager();
        PreviewFragment previewFragment = new PreviewFragment();
        previewFragment.setFragmentManager(fragmentManager);
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, previewFragment)
                .commit();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        MyDeleteDialog myDeleteDialog = new MyDeleteDialog();
        myDeleteDialog.createDialog(this, fragmentManager);
    }

}
