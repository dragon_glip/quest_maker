package com.example.glip.quest_00.adapter;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import com.example.glip.quest_00.R;
import com.example.glip.quest_00.databinding.ChoseStepItemBinding;
import com.example.glip.quest_00.db.entity.StepsEntity;
import com.example.glip.quest_00.helper.OnStartDragListener;
import com.example.glip.quest_00.holder.StepChoseItemHolder;
import java.util.Collections;
import java.util.List;


public class StepChoseItemAdapter extends RecyclerView.Adapter<StepChoseItemHolder> implements ItemTouchHelperAdapter {
    private LayoutInflater layoutInflater;
    private List<StepsEntity> stepsEntityList;
    private ChoseStepItemAdapterListener listener;
    private final OnStartDragListener mDragStartListener;
    StepsEntity deletedStepEntity;

    public StepChoseItemAdapter(ChoseStepItemAdapterListener listener, OnStartDragListener dragStartListener) {
        this.listener = listener;
        this.mDragStartListener = dragStartListener;

    }

    @NonNull
    @Override
    public StepChoseItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        ChoseStepItemBinding binding =
                DataBindingUtil.inflate(layoutInflater, R.layout.chose_step_item, viewGroup, false);
        return new StepChoseItemHolder(binding);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull StepChoseItemHolder stepChoseItemHolder, int i) {
        stepChoseItemHolder.binding.setStepEntity(stepsEntityList.get(i));

        stepChoseItemHolder.binding.reorder.setOnTouchListener((v, event) -> {
            if (event.getAction() ==  MotionEvent.ACTION_DOWN) {
                mDragStartListener.onStartDrag(stepChoseItemHolder);
            }
            return false;
        });
        stepChoseItemHolder.binding.textView99.setOnClickListener(v -> {
            if (listener != null) {
                listener.onStepClicked(stepsEntityList.get(i));
            }
        });
        stepChoseItemHolder.binding.choseStep.setOnClickListener(v -> {
            if (listener != null) {
                listener.onStepClicked(stepsEntityList.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return stepsEntityList != null ? stepsEntityList.size() : 0;
    }

    public void submitList(List<StepsEntity> stepsEntityList) {
        this.stepsEntityList = stepsEntityList;
    }

    @Override
    public boolean onItemMove(int oldPosition, int newPosition) {
//        Collections.swap(stepsEntityList, oldPosition, newPosition);
//        notifyItemMoved(oldPosition, newPosition);
//        return true;
        if (oldPosition < newPosition) {
            for (int i = oldPosition; i < newPosition; i++) {
                Collections.swap(stepsEntityList, i, i + 1);
            }
        } else {
            for (int i = oldPosition; i > newPosition; i--) {
                Collections.swap(stepsEntityList, i, i - 1);
            }
        }
        StepsEntity stepsEntity = stepsEntityList.get(oldPosition);
        stepsEntityList.remove(oldPosition);
        stepsEntityList.add(newPosition, stepsEntity);
        notifyItemMoved(oldPosition, newPosition);

        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        deletedStepEntity = stepsEntityList.get(position);
        stepsEntityList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onItemRestore(int deletedPosition) {
        stepsEntityList.add(deletedPosition, deletedStepEntity);
        notifyItemInserted(deletedPosition);
    }

    public interface ChoseStepItemAdapterListener {

        void onStepClicked(StepsEntity stepsEntity);

        void onDeleteStepClicked(StepsEntity stepsEntity);
    }


}
