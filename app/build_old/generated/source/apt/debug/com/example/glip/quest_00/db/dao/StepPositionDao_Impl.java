package com.example.glip.quest_00.db.dao;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import com.example.glip.quest_00.db.entity.StepPositionEntity;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unchecked")
public class StepPositionDao_Impl implements StepPositionDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfStepPositionEntity;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfStepPositionEntity;

  public StepPositionDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfStepPositionEntity = new EntityInsertionAdapter<StepPositionEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `step_position`(`step_position_id`,`step_id`,`position`,`text`,`url`) VALUES (nullif(?, 0),?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, StepPositionEntity value) {
        stmt.bindLong(1, value.getId());
        stmt.bindLong(2, value.getStepId());
        stmt.bindLong(3, value.getPosition());
        if (value.getText() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getText());
        }
        if (value.getUrl() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getUrl());
        }
      }
    };
    this.__deletionAdapterOfStepPositionEntity = new EntityDeletionOrUpdateAdapter<StepPositionEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `step_position` WHERE `step_position_id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, StepPositionEntity value) {
        stmt.bindLong(1, value.getId());
      }
    };
  }

  @Override
  public long insert(StepPositionEntity stepPositionEntity) {
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfStepPositionEntity.insertAndReturnId(stepPositionEntity);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(StepPositionEntity stepPositionEntity) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfStepPositionEntity.handle(stepPositionEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<StepPositionEntity> getById(String step_id) {
    final String _sql = "SELECT * FROM step_position WHERE step_id LIKE ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (step_id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, step_id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("step_position_id");
      final int _cursorIndexOfStepId = _cursor.getColumnIndexOrThrow("step_id");
      final int _cursorIndexOfPosition = _cursor.getColumnIndexOrThrow("position");
      final int _cursorIndexOfText = _cursor.getColumnIndexOrThrow("text");
      final int _cursorIndexOfUrl = _cursor.getColumnIndexOrThrow("url");
      final List<StepPositionEntity> _result = new ArrayList<StepPositionEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final StepPositionEntity _item;
        final String _tmpText;
        _tmpText = _cursor.getString(_cursorIndexOfText);
        _item = new StepPositionEntity(_tmpText);
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final long _tmpStepId;
        _tmpStepId = _cursor.getLong(_cursorIndexOfStepId);
        _item.setStepId(_tmpStepId);
        final int _tmpPosition;
        _tmpPosition = _cursor.getInt(_cursorIndexOfPosition);
        _item.setPosition(_tmpPosition);
        final String _tmpUrl;
        _tmpUrl = _cursor.getString(_cursorIndexOfUrl);
        _item.setUrl(_tmpUrl);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
