// Generated code from Butter Knife. Do not modify!
package com.example.glip.quest_00.view;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.glip.quest_00.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EnterTextFragment_ViewBinding implements Unbinder {
  private EnterTextFragment target;

  @UiThread
  public EnterTextFragment_ViewBinding(EnterTextFragment target, View source) {
    this.target = target;

    target.textInputQuestion = Utils.findRequiredViewAsType(source, R.id.textInputQuestion, "field 'textInputQuestion'", EditText.class);
    target.buttonOk = Utils.findRequiredViewAsType(source, R.id.ok, "field 'buttonOk'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EnterTextFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.textInputQuestion = null;
    target.buttonOk = null;
  }
}
