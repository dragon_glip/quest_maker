// Generated code from Butter Knife. Do not modify!
package com.example.glip.quest_00.view;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.glip.quest_00.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginFragment_ViewBinding implements Unbinder {
  private LoginFragment target;

  @UiThread
  public LoginFragment_ViewBinding(LoginFragment target, View source) {
    this.target = target;

    target.login = Utils.findRequiredViewAsType(source, R.id.login, "field 'login'", EditText.class);
    target.password = Utils.findRequiredViewAsType(source, R.id.password, "field 'password'", EditText.class);
    target.buttonEnter = Utils.findRequiredViewAsType(source, R.id.enter, "field 'buttonEnter'", Button.class);
    target.buttonRegistration = Utils.findRequiredViewAsType(source, R.id.registration, "field 'buttonRegistration'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.login = null;
    target.password = null;
    target.buttonEnter = null;
    target.buttonRegistration = null;
  }
}
