// Generated code from Butter Knife. Do not modify!
package com.example.glip.quest_00.view;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.glip.quest_00.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class QuestCreateFragment_ViewBinding implements Unbinder {
  private QuestCreateFragment target;

  @UiThread
  public QuestCreateFragment_ViewBinding(QuestCreateFragment target, View source) {
    this.target = target;

    target.questName = Utils.findRequiredViewAsType(source, R.id.qwest_name, "field 'questName'", EditText.class);
    target.happyBirthday = Utils.findRequiredViewAsType(source, R.id.happy_birthday, "field 'happyBirthday'", RadioButton.class);
    target.halloween = Utils.findRequiredViewAsType(source, R.id.halloween, "field 'halloween'", RadioButton.class);
    target.pirate = Utils.findRequiredViewAsType(source, R.id.pirate, "field 'pirate'", RadioButton.class);
    target.create_quest = Utils.findRequiredViewAsType(source, R.id.create_qwest, "field 'create_quest'", Button.class);
    target.textView = Utils.findRequiredViewAsType(source, R.id.textView6, "field 'textView'", TextView.class);
    target.radioGroup = Utils.findRequiredViewAsType(source, R.id.radioGroup, "field 'radioGroup'", RadioGroup.class);
    target.constraintLayout = Utils.findRequiredViewAsType(source, R.id.create_quest_layout, "field 'constraintLayout'", ConstraintLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    QuestCreateFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.questName = null;
    target.happyBirthday = null;
    target.halloween = null;
    target.pirate = null;
    target.create_quest = null;
    target.textView = null;
    target.radioGroup = null;
    target.constraintLayout = null;
  }
}
