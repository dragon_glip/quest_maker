package com.example.glip.quest_00.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Callback;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.room.DatabaseConfiguration;
import android.arch.persistence.room.InvalidationTracker;
import android.arch.persistence.room.RoomOpenHelper;
import android.arch.persistence.room.RoomOpenHelper.Delegate;
import android.arch.persistence.room.util.TableInfo;
import android.arch.persistence.room.util.TableInfo.Column;
import android.arch.persistence.room.util.TableInfo.ForeignKey;
import android.arch.persistence.room.util.TableInfo.Index;
import com.example.glip.quest_00.db.dao.QuestsDao;
import com.example.glip.quest_00.db.dao.QuestsDao_Impl;
import com.example.glip.quest_00.db.dao.StepPositionDao;
import com.example.glip.quest_00.db.dao.StepPositionDao_Impl;
import com.example.glip.quest_00.db.dao.StepsDao;
import com.example.glip.quest_00.db.dao.StepsDao_Impl;
import com.example.glip.quest_00.db.dao.UsersDao;
import com.example.glip.quest_00.db.dao.UsersDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public class DataBaseHelper_Impl extends DataBaseHelper {
  private volatile UsersDao _usersDao;

  private volatile QuestsDao _questsDao;

  private volatile StepsDao _stepsDao;

  private volatile StepPositionDao _stepPositionDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `users` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` TEXT, `password` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `quests` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `user_id` INTEGER NOT NULL, `title` TEXT, `theme` INTEGER NOT NULL, `dateCreatedQuest` INTEGER, `dateModifiedQuest` INTEGER, FOREIGN KEY(`user_id`) REFERENCES `users`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");
        _db.execSQL("CREATE  INDEX `index_quests_user_id` ON `quests` (`user_id`)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `steps` (`step_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `quest_id` INTEGER NOT NULL, `dateCreated` INTEGER, `dateModified` INTEGER, FOREIGN KEY(`quest_id`) REFERENCES `quests`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");
        _db.execSQL("CREATE  INDEX `index_steps_quest_id` ON `steps` (`quest_id`)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `step_position` (`step_position_id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step_id` INTEGER NOT NULL, `position` INTEGER NOT NULL, `text` TEXT, `url` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"f5a3887d82be72520f55608995594ea0\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `users`");
        _db.execSQL("DROP TABLE IF EXISTS `quests`");
        _db.execSQL("DROP TABLE IF EXISTS `steps`");
        _db.execSQL("DROP TABLE IF EXISTS `step_position`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        _db.execSQL("PRAGMA foreign_keys = ON");
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsUsers = new HashMap<String, TableInfo.Column>(3);
        _columnsUsers.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsUsers.put("name", new TableInfo.Column("name", "TEXT", false, 0));
        _columnsUsers.put("password", new TableInfo.Column("password", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysUsers = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesUsers = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoUsers = new TableInfo("users", _columnsUsers, _foreignKeysUsers, _indicesUsers);
        final TableInfo _existingUsers = TableInfo.read(_db, "users");
        if (! _infoUsers.equals(_existingUsers)) {
          throw new IllegalStateException("Migration didn't properly handle users(com.example.glip.quest_00.db.entity.UsersEntity).\n"
                  + " Expected:\n" + _infoUsers + "\n"
                  + " Found:\n" + _existingUsers);
        }
        final HashMap<String, TableInfo.Column> _columnsQuests = new HashMap<String, TableInfo.Column>(6);
        _columnsQuests.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsQuests.put("user_id", new TableInfo.Column("user_id", "INTEGER", true, 0));
        _columnsQuests.put("title", new TableInfo.Column("title", "TEXT", false, 0));
        _columnsQuests.put("theme", new TableInfo.Column("theme", "INTEGER", true, 0));
        _columnsQuests.put("dateCreatedQuest", new TableInfo.Column("dateCreatedQuest", "INTEGER", false, 0));
        _columnsQuests.put("dateModifiedQuest", new TableInfo.Column("dateModifiedQuest", "INTEGER", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysQuests = new HashSet<TableInfo.ForeignKey>(1);
        _foreignKeysQuests.add(new TableInfo.ForeignKey("users", "CASCADE", "NO ACTION",Arrays.asList("user_id"), Arrays.asList("id")));
        final HashSet<TableInfo.Index> _indicesQuests = new HashSet<TableInfo.Index>(1);
        _indicesQuests.add(new TableInfo.Index("index_quests_user_id", false, Arrays.asList("user_id")));
        final TableInfo _infoQuests = new TableInfo("quests", _columnsQuests, _foreignKeysQuests, _indicesQuests);
        final TableInfo _existingQuests = TableInfo.read(_db, "quests");
        if (! _infoQuests.equals(_existingQuests)) {
          throw new IllegalStateException("Migration didn't properly handle quests(com.example.glip.quest_00.db.entity.QuestsEntity).\n"
                  + " Expected:\n" + _infoQuests + "\n"
                  + " Found:\n" + _existingQuests);
        }
        final HashMap<String, TableInfo.Column> _columnsSteps = new HashMap<String, TableInfo.Column>(4);
        _columnsSteps.put("step_id", new TableInfo.Column("step_id", "INTEGER", true, 1));
        _columnsSteps.put("quest_id", new TableInfo.Column("quest_id", "INTEGER", true, 0));
        _columnsSteps.put("dateCreated", new TableInfo.Column("dateCreated", "INTEGER", false, 0));
        _columnsSteps.put("dateModified", new TableInfo.Column("dateModified", "INTEGER", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysSteps = new HashSet<TableInfo.ForeignKey>(1);
        _foreignKeysSteps.add(new TableInfo.ForeignKey("quests", "CASCADE", "NO ACTION",Arrays.asList("quest_id"), Arrays.asList("id")));
        final HashSet<TableInfo.Index> _indicesSteps = new HashSet<TableInfo.Index>(1);
        _indicesSteps.add(new TableInfo.Index("index_steps_quest_id", false, Arrays.asList("quest_id")));
        final TableInfo _infoSteps = new TableInfo("steps", _columnsSteps, _foreignKeysSteps, _indicesSteps);
        final TableInfo _existingSteps = TableInfo.read(_db, "steps");
        if (! _infoSteps.equals(_existingSteps)) {
          throw new IllegalStateException("Migration didn't properly handle steps(com.example.glip.quest_00.db.entity.StepsEntity).\n"
                  + " Expected:\n" + _infoSteps + "\n"
                  + " Found:\n" + _existingSteps);
        }
        final HashMap<String, TableInfo.Column> _columnsStepPosition = new HashMap<String, TableInfo.Column>(5);
        _columnsStepPosition.put("step_position_id", new TableInfo.Column("step_position_id", "INTEGER", true, 1));
        _columnsStepPosition.put("step_id", new TableInfo.Column("step_id", "INTEGER", true, 0));
        _columnsStepPosition.put("position", new TableInfo.Column("position", "INTEGER", true, 0));
        _columnsStepPosition.put("text", new TableInfo.Column("text", "TEXT", false, 0));
        _columnsStepPosition.put("url", new TableInfo.Column("url", "TEXT", false, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysStepPosition = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesStepPosition = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoStepPosition = new TableInfo("step_position", _columnsStepPosition, _foreignKeysStepPosition, _indicesStepPosition);
        final TableInfo _existingStepPosition = TableInfo.read(_db, "step_position");
        if (! _infoStepPosition.equals(_existingStepPosition)) {
          throw new IllegalStateException("Migration didn't properly handle step_position(com.example.glip.quest_00.db.entity.StepPositionEntity).\n"
                  + " Expected:\n" + _infoStepPosition + "\n"
                  + " Found:\n" + _existingStepPosition);
        }
      }
    }, "f5a3887d82be72520f55608995594ea0", "e923e64d02cee3056458b1121c19ee9b");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "users","quests","steps","step_position");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    boolean _supportsDeferForeignKeys = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP;
    try {
      if (!_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA foreign_keys = FALSE");
      }
      super.beginTransaction();
      if (_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA defer_foreign_keys = TRUE");
      }
      _db.execSQL("DELETE FROM `users`");
      _db.execSQL("DELETE FROM `quests`");
      _db.execSQL("DELETE FROM `steps`");
      _db.execSQL("DELETE FROM `step_position`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      if (!_supportsDeferForeignKeys) {
        _db.execSQL("PRAGMA foreign_keys = TRUE");
      }
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public UsersDao getUsersDao() {
    if (_usersDao != null) {
      return _usersDao;
    } else {
      synchronized(this) {
        if(_usersDao == null) {
          _usersDao = new UsersDao_Impl(this);
        }
        return _usersDao;
      }
    }
  }

  @Override
  public QuestsDao getQuestsDao() {
    if (_questsDao != null) {
      return _questsDao;
    } else {
      synchronized(this) {
        if(_questsDao == null) {
          _questsDao = new QuestsDao_Impl(this);
        }
        return _questsDao;
      }
    }
  }

  @Override
  public StepsDao getStepsDao() {
    if (_stepsDao != null) {
      return _stepsDao;
    } else {
      synchronized(this) {
        if(_stepsDao == null) {
          _stepsDao = new StepsDao_Impl(this);
        }
        return _stepsDao;
      }
    }
  }

  @Override
  public StepPositionDao getStepPositionDao() {
    if (_stepPositionDao != null) {
      return _stepPositionDao;
    } else {
      synchronized(this) {
        if(_stepPositionDao == null) {
          _stepPositionDao = new StepPositionDao_Impl(this);
        }
        return _stepPositionDao;
      }
    }
  }
}
