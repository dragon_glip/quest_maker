package com.example.glip.quest_00.db.dao;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.arch.persistence.room.RxRoom;
import android.database.Cursor;
import com.example.glip.quest_00.db.converter.DateConverter;
import com.example.glip.quest_00.db.entity.QuestsEntity;
import io.reactivex.Flowable;
import java.lang.Exception;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings("unchecked")
public class QuestsDao_Impl implements QuestsDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfQuestsEntity;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfQuestsEntity;

  public QuestsDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfQuestsEntity = new EntityInsertionAdapter<QuestsEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `quests`(`id`,`user_id`,`title`,`theme`,`dateCreatedQuest`,`dateModifiedQuest`) VALUES (nullif(?, 0),?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, QuestsEntity value) {
        stmt.bindLong(1, value.getId());
        stmt.bindLong(2, value.getUserId());
        if (value.getTitle() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getTitle());
        }
        stmt.bindLong(4, value.getTheme());
        final Long _tmp;
        _tmp = DateConverter.toTimestamp(value.getDateCreatedQuest());
        if (_tmp == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindLong(5, _tmp);
        }
        final Long _tmp_1;
        _tmp_1 = DateConverter.toTimestamp(value.getDateModifiedQuest());
        if (_tmp_1 == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindLong(6, _tmp_1);
        }
      }
    };
    this.__deletionAdapterOfQuestsEntity = new EntityDeletionOrUpdateAdapter<QuestsEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `quests` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, QuestsEntity value) {
        stmt.bindLong(1, value.getId());
      }
    };
  }

  @Override
  public long insertQuest(QuestsEntity questsEntity) {
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfQuestsEntity.insertAndReturnId(questsEntity);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(QuestsEntity questsEntity) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfQuestsEntity.handle(questsEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<QuestsEntity> getAllData() {
    final String _sql = "SELECT * FROM quests";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
      final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
      final int _cursorIndexOfTheme = _cursor.getColumnIndexOrThrow("theme");
      final int _cursorIndexOfDateCreatedQuest = _cursor.getColumnIndexOrThrow("dateCreatedQuest");
      final int _cursorIndexOfDateModifiedQuest = _cursor.getColumnIndexOrThrow("dateModifiedQuest");
      final List<QuestsEntity> _result = new ArrayList<QuestsEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final QuestsEntity _item;
        _item = new QuestsEntity();
        final long _tmpId;
        _tmpId = _cursor.getLong(_cursorIndexOfId);
        _item.setId(_tmpId);
        final int _tmpUserId;
        _tmpUserId = _cursor.getInt(_cursorIndexOfUserId);
        _item.setUserId(_tmpUserId);
        final String _tmpTitle;
        _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
        _item.setTitle(_tmpTitle);
        final int _tmpTheme;
        _tmpTheme = _cursor.getInt(_cursorIndexOfTheme);
        _item.setTheme(_tmpTheme);
        final Date _tmpDateCreatedQuest;
        final Long _tmp;
        if (_cursor.isNull(_cursorIndexOfDateCreatedQuest)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getLong(_cursorIndexOfDateCreatedQuest);
        }
        _tmpDateCreatedQuest = DateConverter.toDate(_tmp);
        _item.setDateCreatedQuest(_tmpDateCreatedQuest);
        final Date _tmpDateModifiedQuest;
        final Long _tmp_1;
        if (_cursor.isNull(_cursorIndexOfDateModifiedQuest)) {
          _tmp_1 = null;
        } else {
          _tmp_1 = _cursor.getLong(_cursorIndexOfDateModifiedQuest);
        }
        _tmpDateModifiedQuest = DateConverter.toDate(_tmp_1);
        _item.setDateModifiedQuest(_tmpDateModifiedQuest);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Flowable<QuestsEntity> getByTitle(String title) {
    final String _sql = "SELECT * FROM quests WHERE title LIKE ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (title == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, title);
    }
    return RxRoom.createFlowable(__db, new String[]{"quests"}, new Callable<QuestsEntity>() {
      @Override
      public QuestsEntity call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfTheme = _cursor.getColumnIndexOrThrow("theme");
          final int _cursorIndexOfDateCreatedQuest = _cursor.getColumnIndexOrThrow("dateCreatedQuest");
          final int _cursorIndexOfDateModifiedQuest = _cursor.getColumnIndexOrThrow("dateModifiedQuest");
          final QuestsEntity _result;
          if(_cursor.moveToFirst()) {
            _result = new QuestsEntity();
            final long _tmpId;
            _tmpId = _cursor.getLong(_cursorIndexOfId);
            _result.setId(_tmpId);
            final int _tmpUserId;
            _tmpUserId = _cursor.getInt(_cursorIndexOfUserId);
            _result.setUserId(_tmpUserId);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            _result.setTitle(_tmpTitle);
            final int _tmpTheme;
            _tmpTheme = _cursor.getInt(_cursorIndexOfTheme);
            _result.setTheme(_tmpTheme);
            final Date _tmpDateCreatedQuest;
            final Long _tmp;
            if (_cursor.isNull(_cursorIndexOfDateCreatedQuest)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getLong(_cursorIndexOfDateCreatedQuest);
            }
            _tmpDateCreatedQuest = DateConverter.toDate(_tmp);
            _result.setDateCreatedQuest(_tmpDateCreatedQuest);
            final Date _tmpDateModifiedQuest;
            final Long _tmp_1;
            if (_cursor.isNull(_cursorIndexOfDateModifiedQuest)) {
              _tmp_1 = null;
            } else {
              _tmp_1 = _cursor.getLong(_cursorIndexOfDateModifiedQuest);
            }
            _tmpDateModifiedQuest = DateConverter.toDate(_tmp_1);
            _result.setDateModifiedQuest(_tmpDateModifiedQuest);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public Flowable<QuestsEntity> getById(long id) {
    final String _sql = "SELECT * FROM quests WHERE id LIKE ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    return RxRoom.createFlowable(__db, new String[]{"quests"}, new Callable<QuestsEntity>() {
      @Override
      public QuestsEntity call() throws Exception {
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfUserId = _cursor.getColumnIndexOrThrow("user_id");
          final int _cursorIndexOfTitle = _cursor.getColumnIndexOrThrow("title");
          final int _cursorIndexOfTheme = _cursor.getColumnIndexOrThrow("theme");
          final int _cursorIndexOfDateCreatedQuest = _cursor.getColumnIndexOrThrow("dateCreatedQuest");
          final int _cursorIndexOfDateModifiedQuest = _cursor.getColumnIndexOrThrow("dateModifiedQuest");
          final QuestsEntity _result;
          if(_cursor.moveToFirst()) {
            _result = new QuestsEntity();
            final long _tmpId;
            _tmpId = _cursor.getLong(_cursorIndexOfId);
            _result.setId(_tmpId);
            final int _tmpUserId;
            _tmpUserId = _cursor.getInt(_cursorIndexOfUserId);
            _result.setUserId(_tmpUserId);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            _result.setTitle(_tmpTitle);
            final int _tmpTheme;
            _tmpTheme = _cursor.getInt(_cursorIndexOfTheme);
            _result.setTheme(_tmpTheme);
            final Date _tmpDateCreatedQuest;
            final Long _tmp;
            if (_cursor.isNull(_cursorIndexOfDateCreatedQuest)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getLong(_cursorIndexOfDateCreatedQuest);
            }
            _tmpDateCreatedQuest = DateConverter.toDate(_tmp);
            _result.setDateCreatedQuest(_tmpDateCreatedQuest);
            final Date _tmpDateModifiedQuest;
            final Long _tmp_1;
            if (_cursor.isNull(_cursorIndexOfDateModifiedQuest)) {
              _tmp_1 = null;
            } else {
              _tmp_1 = _cursor.getLong(_cursorIndexOfDateModifiedQuest);
            }
            _tmpDateModifiedQuest = DateConverter.toDate(_tmp_1);
            _result.setDateModifiedQuest(_tmpDateModifiedQuest);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
