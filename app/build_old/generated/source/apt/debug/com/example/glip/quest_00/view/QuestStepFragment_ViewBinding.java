// Generated code from Butter Knife. Do not modify!
package com.example.glip.quest_00.view;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.example.glip.quest_00.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class QuestStepFragment_ViewBinding implements Unbinder {
  private QuestStepFragment target;

  @UiThread
  public QuestStepFragment_ViewBinding(QuestStepFragment target, View source) {
    this.target = target;

    target.radioGroupSetQuestion = Utils.findRequiredViewAsType(source, R.id.radioGroupSetQuestion, "field 'radioGroupSetQuestion'", RadioGroup.class);
    target.audioFile = Utils.findRequiredViewAsType(source, R.id.audio_file, "field 'audioFile'", RadioButton.class);
    target.videoFile = Utils.findRequiredViewAsType(source, R.id.video_file, "field 'videoFile'", RadioButton.class);
    target.pictureFile = Utils.findRequiredViewAsType(source, R.id.picture_file, "field 'pictureFile'", RadioButton.class);
    target.textFile = Utils.findRequiredViewAsType(source, R.id.text_file, "field 'textFile'", RadioButton.class);
    target.downloadFile = Utils.findRequiredViewAsType(source, R.id.download_file, "field 'downloadFile'", Button.class);
    target.variableNextStep = Utils.findRequiredViewAsType(source, R.id.variable_next_step, "field 'variableNextStep'", EditText.class);
    target.saveStep = Utils.findRequiredViewAsType(source, R.id.save, "field 'saveStep'", Button.class);
    target.saveAndCreateNewStep = Utils.findRequiredViewAsType(source, R.id.save_and_create_step, "field 'saveAndCreateNewStep'", Button.class);
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    target.titleQuest = Utils.findRequiredViewAsType(source, R.id.title_quest, "field 'titleQuest'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    QuestStepFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.radioGroupSetQuestion = null;
    target.audioFile = null;
    target.videoFile = null;
    target.pictureFile = null;
    target.textFile = null;
    target.downloadFile = null;
    target.variableNextStep = null;
    target.saveStep = null;
    target.saveAndCreateNewStep = null;
    target.recyclerView = null;
    target.titleQuest = null;
  }
}
