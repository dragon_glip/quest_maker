package com.example.glip.quest_00.db.dao;

import android.arch.persistence.db.SupportSQLiteStatement;
import android.arch.persistence.room.EntityDeletionOrUpdateAdapter;
import android.arch.persistence.room.EntityInsertionAdapter;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.RoomSQLiteQuery;
import android.database.Cursor;
import com.example.glip.quest_00.db.converter.DateConverter;
import com.example.glip.quest_00.db.entity.StepsEntity;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unchecked")
public class StepsDao_Impl implements StepsDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfStepsEntity;

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfStepsEntity;

  public StepsDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfStepsEntity = new EntityInsertionAdapter<StepsEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR ABORT INTO `steps`(`step_id`,`quest_id`,`dateCreated`,`dateModified`) VALUES (nullif(?, 0),?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, StepsEntity value) {
        stmt.bindLong(1, value.getStepId());
        stmt.bindLong(2, value.getQuestId());
        final Long _tmp;
        _tmp = DateConverter.toTimestamp(value.getDateCreated());
        if (_tmp == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, _tmp);
        }
        final Long _tmp_1;
        _tmp_1 = DateConverter.toTimestamp(value.getDateModified());
        if (_tmp_1 == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindLong(4, _tmp_1);
        }
      }
    };
    this.__deletionAdapterOfStepsEntity = new EntityDeletionOrUpdateAdapter<StepsEntity>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `steps` WHERE `step_id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, StepsEntity value) {
        stmt.bindLong(1, value.getStepId());
      }
    };
  }

  @Override
  public void insert(StepsEntity stepsEntity) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfStepsEntity.insert(stepsEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(StepsEntity stepsEntity) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfStepsEntity.handle(stepsEntity);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public List<StepsEntity> getAllData() {
    final String _sql = "SELECT * FROM steps";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfStepId = _cursor.getColumnIndexOrThrow("step_id");
      final int _cursorIndexOfQuestId = _cursor.getColumnIndexOrThrow("quest_id");
      final int _cursorIndexOfDateCreated = _cursor.getColumnIndexOrThrow("dateCreated");
      final int _cursorIndexOfDateModified = _cursor.getColumnIndexOrThrow("dateModified");
      final List<StepsEntity> _result = new ArrayList<StepsEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final StepsEntity _item;
        _item = new StepsEntity();
        final int _tmpStepId;
        _tmpStepId = _cursor.getInt(_cursorIndexOfStepId);
        _item.setStepId(_tmpStepId);
        final int _tmpQuestId;
        _tmpQuestId = _cursor.getInt(_cursorIndexOfQuestId);
        _item.setQuestId(_tmpQuestId);
        final Date _tmpDateCreated;
        final Long _tmp;
        if (_cursor.isNull(_cursorIndexOfDateCreated)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getLong(_cursorIndexOfDateCreated);
        }
        _tmpDateCreated = DateConverter.toDate(_tmp);
        _item.setDateCreated(_tmpDateCreated);
        final Date _tmpDateModified;
        final Long _tmp_1;
        if (_cursor.isNull(_cursorIndexOfDateModified)) {
          _tmp_1 = null;
        } else {
          _tmp_1 = _cursor.getLong(_cursorIndexOfDateModified);
        }
        _tmpDateModified = DateConverter.toDate(_tmp_1);
        _item.setDateModified(_tmpDateModified);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<StepsEntity> getByTitle(String quest_id) {
    final String _sql = "SELECT * FROM steps WHERE quest_id LIKE ?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (quest_id == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, quest_id);
    }
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfStepId = _cursor.getColumnIndexOrThrow("step_id");
      final int _cursorIndexOfQuestId = _cursor.getColumnIndexOrThrow("quest_id");
      final int _cursorIndexOfDateCreated = _cursor.getColumnIndexOrThrow("dateCreated");
      final int _cursorIndexOfDateModified = _cursor.getColumnIndexOrThrow("dateModified");
      final List<StepsEntity> _result = new ArrayList<StepsEntity>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final StepsEntity _item;
        _item = new StepsEntity();
        final int _tmpStepId;
        _tmpStepId = _cursor.getInt(_cursorIndexOfStepId);
        _item.setStepId(_tmpStepId);
        final int _tmpQuestId;
        _tmpQuestId = _cursor.getInt(_cursorIndexOfQuestId);
        _item.setQuestId(_tmpQuestId);
        final Date _tmpDateCreated;
        final Long _tmp;
        if (_cursor.isNull(_cursorIndexOfDateCreated)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getLong(_cursorIndexOfDateCreated);
        }
        _tmpDateCreated = DateConverter.toDate(_tmp);
        _item.setDateCreated(_tmpDateCreated);
        final Date _tmpDateModified;
        final Long _tmp_1;
        if (_cursor.isNull(_cursorIndexOfDateModified)) {
          _tmp_1 = null;
        } else {
          _tmp_1 = _cursor.getLong(_cursorIndexOfDateModified);
        }
        _tmpDateModified = DateConverter.toDate(_tmp_1);
        _item.setDateModified(_tmpDateModified);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
